function pass = generatePassword(n)
  ## Randomize a new password of length n
  ##
  ## Inputs:
  ##          n     -   The number of characters required
  ## Outputs:
  ##          pass  -   The generated Password
  
  pass = "";
  
  x = ceil(62 * rand(1, n));
  
  for i = 1:n
    y = x(1, i);
    if y <= 10
      pass = strcat(pass, char(y + 47));
    elseif y <= 36
      pass = strcat(pass, char(y + 54));
    else
      pass = strcat(pass, char(y + 60));
    endif
  endfor
  
endfunction