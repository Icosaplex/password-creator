function charCounts = countLetters(data)
  
  charCounts = [];
  
  # Count Digits
  for i = 48:57
    charCounts = vertcat(charCounts, length(strfind(data, char(i))));
  endfor
  
  #Count caps
  for i = 65:90
    charCounts = vertcat(charCounts, length(strfind(data, char(i))));
  endfor
  
  #count lowers
  for i = 97:122
    charCounts = vertcat(charCounts, length(strfind(data, char(i))));
  endfor

endfunction